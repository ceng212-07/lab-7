**CENG212, Fall 2015**  
**Lab 7**

Using Eclipse project format.

Thomas, I've added the files from my previous labs that will be needed for lab
7. Feel free to replace them if you want.


**Style Guide**

Currently, I use tabs for indentation and keep my lines less than 80 characters
long. This is mainly because I program in VIM, in the terminal, and usually have
more than one code window open.

We should either use a similar coding style or never work on the same classes.
I think I'd rather we use the same coding style.


**Error checking**

Interface:

* Only letters can be entered in the Name, Territory, and Department fields.
* Only numbers can be entered in the Sales Target and employee level fields.

Back-end:

* Departments must be one of the following: Operations, Development, Quality Assurance.
* Employee levels must be between 1 and 3.
* Territories must be one of the following: North, West, South, East.
* Employee numbers must be unique.


**Error codes**

Company.addEmployee returns -1 if attempting to add a type of employee that
does not exist.
It returns 1 if attempting to add an employee with invalid parameters (e.g.
a territory, department, or level that does not exist). Returns 0 if no errors.
